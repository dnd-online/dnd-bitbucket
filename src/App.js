// import logo from './logo.svg';
import './App.css';
import { HeroConstructor } from './content/components/HeroConstructor.jsx/HeroConstructor';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './content/layout/Login/Login';
import Header from './content/layout/Header/Header';
import Home from './content/components/Home/Home';


function App() {
  
  return (
    <div className="App">
      <Header />
      <BrowserRouter>
        <Switch>
        <Route exact path="/">
            <Home />
          </Route>
          <Route path="/heroconstructor">
            <HeroConstructor></HeroConstructor>
          </Route>
          <Route path="/login">
            <Login></Login>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
