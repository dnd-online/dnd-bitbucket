import React, { useState } from 'react'
import { auth } from '../../config/firebase/firebaseConfig'
import firebase from 'firebase/app'

export default function Header(props) {

    
    const [isAuthenticated, setAuthenticated] = useState(false)
    

    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            setAuthenticated(true)
        } else {
            setAuthenticated(false)

        }
    });

    const logout = () => {
        auth.signOut();
    }

    const login = () => {
        window.location = '/login'
    }

    return (
        <header>
            <h1>DnD Online</h1>
            {isAuthenticated ?
                <button onClick={() => { logout() }}>Sign out</button> :
                <button onClick={() => { login() }}>Sign in</button>}

        </header>
    )
}
