import React from 'react'
import './dist/TextField.css'

export default function TextField(props) {

    const { requiredField, label, name } = props;

    return (
        <div>
            <span className="field-label"></span>{label}
            <input type="text" required={requiredField ? true : false} name={name} id={name} />
        </div>
    )
}
