import React, { Component } from 'react'
import TextField from '../TextField/TextField'
import { FirebaseAuthConsumer, FirebaseAuthProvider } from '@react-firebase/auth';
import { firebaseConfig, provider, auth } from '../../config/firebase/firebaseConfig'
import firebase from "firebase/app";
import { Redirect } from 'react-router-dom';

export class Login extends Component {

     signInWithGoogle = () => {
        auth.signInWithPopup(provider).then(() => {
            console.log('Signed in!');
            window.location = '/'
        });
      };



    render() {

        return (<div>
            <button onClick={() => {this.signInWithGoogle()}}>Login with Google</button>
        </div>
        )
    }
}

export default Login
