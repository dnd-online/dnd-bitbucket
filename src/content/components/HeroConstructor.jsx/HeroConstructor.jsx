import React, { Component } from 'react'
import TextField from '../../layout/TextField/TextField'
import $ from 'jquery'

export class HeroConstructor extends Component {

    handleForm = (e) => {
        e.preventDefault();
        // Save this player thingy somehwere in database?
        console.log($(e.target).serialize());
    }

    render() {

        // Fields to fill
        
        const nameField = {
            label: 'Enter name:',
            name: 'name',
            requiredField: true
        }
        const labelRace = {
            label: 'Enter race:',
            name: 'race',
            requiredField: true
        }
        const labelClass = {
            label: 'Enter class:',
            name: 'class',
            requiredField: true
        }

        return (
            <section>
                <form data-js-hero-constructor-form onSubmit={(e) => {this.handleForm(e)}}>
                    <TextField {...nameField}></TextField>
                    <TextField {...labelRace} ></TextField>
                    <TextField {...labelClass} ></TextField>

                    <button type="submit">Submit</button>
                </form>

            </section>
        )
    }
}

