import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { auth } from '../../config/firebase/firebaseConfig'
import firebase from 'firebase/app'
export class Home extends Component {

    constructor(props) {
        super(props)
        this.state = { signedIn: false }
    }

    componentDidMount() {
        firebase.auth().onAuthStateChanged( (user) => {
            if (user) {
                this.setState({ signedIn: true })
                console.log(this.state);
            } else {
                this.setState({ signedIn: false })
            }
        });

    }
   
    askToSignIn = () => {
        return (<div>
            <h2>Welcome to DnD Online!</h2>
            <p>Sign in to begin your adventure</p>
            <Link to="/login">Sign in with Google</Link>
        </div>)
    }

    signedInView = () => {
        return (
            <div className="start-page">
                <h2>Welcome, traveller</h2>
                <p>Let's set up your hero or choose one that you already have!</p>
            </div>
        )
    }


    render() {
        return (
            <div>
                {this.state.signedIn ? this.signedInView() : this.askToSignIn()}
            </div>
        )
    }
}

export default Home
